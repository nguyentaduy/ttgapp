from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from rest_framework import viewsets
from rest_framework.decorators import list_route

from .models import Game
from .serializers import GameSerializer
from .utils import *
# Create your views here.


def assign(request):
    userid = request.GET.get('userid')
    roomid = request.GET.get('roomid', default='default')
    # username = request.GET.get('username')
    if request.method == 'GET':
        gameid, weight, time = get_game(userid, roomid)
        data = {"gameid" : gameid, "weight" : weight, "time": time}
        serializer = GameSerializer(data)
        return JsonResponse(serializer.data, safe=False)

def checkProposal(request):
    gameid = request.GET.get('gameid')
    res = check_proposal(gameid)
    return JsonResponse(res, safe=False)


def checkEndGame(request):
    gameid = request.GET.get('gameid')
    res = check_endgame(gameid)
    return JsonResponse(res, safe=False)
