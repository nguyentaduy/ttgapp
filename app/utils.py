import scipy as sp
import numpy as np
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import auth
import uuid
from datetime import datetime
cred = credentials.Certificate("app/key2.json")
firebase_admin.initialize_app(cred)
from random import randint

def get_game(userid, roomid):
    db = firestore.client()
    transaction = db.transaction()
    @firestore.transactional
    def get(transaction, userid, roomid):
        try:
            user = auth.get_user(userid)
            userInfo = db.collection(u'users').document(userid)
            existing_user = False
            gameid = ''
            try:
                pendingGame = userInfo.get(transaction=transaction)
                existing_user = True
            except Exception:
                existing_user = False
            if existing_user:
                try:
                    all_games = pendingGame.get(u'all_games')
                    if roomid in all_games:
                        gameid = all_games[roomid][u'gameid']
                        weight = all_games[roomid][u'weight']
                except Exception:
                    gameid = ''
                if gameid is not '':
                    g = db.collection(u'games').document(gameid).get(transaction=transaction)
                    try:
                        u = db.collection(u'games').document(gameid).collection(u'users').document(userid).get(transaction=transaction)
                        stt = u.get(u'status')
                        if g.get(u'status') == 'finished' or stt == 'inPlay':
                            inPlay = True
                        else:
                            inPlay = False
                    except Exception:
                        inPlay = False
                    time = float(g.get(u'timeStart'))
                    now = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
                    if inPlay and now - time < 5 * 60 * 1000:
                        return gameid, weight, time
                    else:
                        gameid = ''
            if gameid == '':
                w_games = db.collection(u'waiting_games') \
                            .where("status", "==", "waiting") \
                            .limit(1) \
                            .get(transaction=transaction)
                w_games_to_list = []
                for g in w_games:
                    w_games_to_list.append(g)
                gameid = w_games_to_list[randint(0, len(w_games_to_list) - 1)].id

            w_g = db.collection(u'waiting_games').document(gameid)
            waiting_game = w_g.get(transaction=transaction).to_dict()
            num_players = int(waiting_game[u'num_players'])
            assigned_players = waiting_game[u'assigned_players']
            thresholds = waiting_game[u'thresholds']
            values = waiting_game[u'values']
            weights = waiting_game[u'weights']
            timeAssigned = waiting_game[u'timeAssigned']
            # If existing user
            # first update time assigned
            if userid in assigned_players:
                timeAssigned[assigned_players.index(userid)] = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
            # Otherwise
            elif len(assigned_players) < len(weights):
                assigned_players.append(userid)
                timeAssigned.append(int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000))
            else:
                i = timeAssigned.index(min(timeAssigned))
                assigned_players[i] = userid
                timeAssigned[i] = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
            rank = assigned_players.index(userid)
            weight = weights[rank]
            t = timeAssigned[rank]
            now = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
            if len(assigned_players) == num_players and now - min(timeAssigned) < 5 * 60 * 1000:
                transaction.update(w_g, {u'status' : u'played', \
                u'assigned_players' : assigned_players, \
                u'timeAssigned': timeAssigned})
            else:
                transaction.update(w_g, {u'assigned_players' : assigned_players, \
                u'timeAssigned': timeAssigned})

            if len(assigned_players) == 1:
                status = u'waiting'
                timestamp = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
                data = {u'status' : status,
                        u'thresholds' : thresholds,
                        u'values': values,
                        u'weights': weights,
                        u'timeStart' : timestamp,
                        u'roomid': roomid,
                        u'pendingPropose': 0}
                new_game = db.collection(u'games').document(gameid)
                transaction.set(new_game, data)
            nicknames = ["Red Cat", "Blue Cat", "Green Cat", "Yellow Cat", "Brown Cat"]
            if len(assigned_players) == num_players and now - min(timeAssigned) < 5 * 60 * 1000:
                timestamp = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
                game = db.collection(u'games').document(gameid)
                transaction.update(game, {u'status' : u'active', u'timeStart' : timestamp})
                for rank in range(len(weights)):
                    nickname = nicknames[rank]
                    userdata = {
                                u'score' : 0,
                                u'stamina' : weights[rank],
                                u'status' : 'inPlay',
                                u'uid' : assigned_players[rank],
                                u'nickname' : nicknames[rank],
                                u'nextProposal': timestamp + 15000}
                    users = db.collection(u'games').document(gameid).collection(u'users').document(assigned_players[rank])
                    transaction.set(users, userdata)
            # update user info
            if not existing_user:
                history = db.collection(u'users').document(userid).collection(u'history').document(gameid)
                transaction.set(history, {u'gameroom': roomid, u'gameid': gameid, u'weight': weight})
                transaction.set(userInfo, \
                {u'all_games.' + roomid: {u'gameid': gameid, u'weight': weight}})
            else:
                history = db.collection(u'users').document(userid).collection(u'history').document(gameid)
                transaction.set(history, {u'gameroom': roomid, u'gameid': gameid, u'weight': weight})
                transaction.update(userInfo, \
                {u'all_games.' + roomid: {u'gameid': gameid, u'weight': weight}})
            return gameid, weight, t
        except Exception:
            return "error", -1, -1
    return get(transaction, userid, roomid)
def check_proposal(gameid):
    db = firestore.client()
    transaction = db.transaction()
    @firestore.transactional
    def check(transaction, gameid):
        try:
            all_proposals = db.collection(u'games').document(gameid).collection(u'pendingPropose').get(transaction=transaction)
            game = db.collection(u'games').document(gameid).get(transaction=transaction).to_dict()
            users = db.collection(u'games').document(gameid).collection(u'users').get(transaction=transaction)
            roomid = game[u'roomid']
            timeCreated = 0
            out_of_date = False
            rejected = False
            accepted = True
            history = []
            taskName = '0'
            ids = []
            uids = []
            scores = {}
            senderid = ''
            for p in all_proposals:
                ids.append(p.id)
                proposal = p.to_dict()
                history.append(proposal)
                timeCreated = proposal['timeCreated']
                taskName = proposal['taskName']
                scores[proposal['uid']] = proposal['share']
                if proposal['sender'] == proposal['name']:
                    senderid = proposal['uid']
                else:
                    uids.append(proposal['uid'])
                if proposal['response'] != 'Yes':
                    accepted = False
                if proposal['response'] == 'No':
                    rejected = True
            now = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
            out_of_date = (int(timeCreated) + 60000 - now) < 0
            new_item = db.collection(u'games').document(gameid).collection(u'proposeHistory').document(str(timeCreated))
            if accepted and len(history) > 0:
                remainders = []
                remaindersWeight = 0
                for u in users:
                    uu = u.to_dict()
                    if not (uu[u'uid'] in uids or uu['uid'] == senderid or uu['status'] == 'outPlay'):
                        remainders.append(uu[u'uid'])
                        remaindersWeight += uu[u'stamina']
                # print(game['thresholds'])
                if remaindersWeight < min(game['thresholds']):
                    for i in remainders:
                        u = db.collection(u'games').document(gameid).collection(u'users').document(i)
                        uu = db.collection(u'users').document(i)
                        transaction.update(u, {"status": 'outPlay', "score": 0})
                        transaction.update(uu, {u'all_games.' + roomid: {u'gameid' : "", u'weight': 0}})
                    transaction.update(db.collection(u'games').document(gameid), {"status": "finished"})
                for p in ids:
                    g = db.collection(u'games').document(gameid).collection(u'pendingPropose').document(p)
                    transaction.delete(g)
                transaction.set(new_item, {
                'history': history,
                'result': 'Accepted',
                'taskName': taskName
                })
                for i in uids:
                    u = db.collection(u'games').document(gameid).collection(u'users').document(i)
                    uu = db.collection(u'users').document(i)
                    # s = db.collection(u'users').document(i).collection(u'scores').document(gameid)
                    transaction.update(u, {"status": 'outPlay', "score": scores[i]})
                    transaction.update(uu, {u'all_games.' + roomid: {u'gameid' : "", u'weight': 0}})
                    # transaction.set(s, {"score": scores[i]})
                if senderid != '':
                    u = db.collection(u'games').document(gameid).collection(u'users').document(senderid)
                    uu = db.collection(u'users').document(senderid)
                    # s = db.collection(u'users').document(senderid).collection(u'scores').document(gameid)
                    transaction.update(u, {"status": 'outPlay', "score": scores[senderid]})
                    transaction.update(uu, {u'all_games.' + roomid: {u'gameid' : "", u'weight': 0}})
                    # transaction.set(s, {"score": scores[senderid]})
                u = db.collection(u'games').document(gameid)
                transaction.update(u, {u'pendingPropose': 0})
            elif rejected and len(history) > 0:
                for p in ids:
                    g = db.collection(u'games').document(gameid).collection(u'pendingPropose').document(p)
                    transaction.delete(g)
                for i in uids:
                    u = db.collection(u'games').document(gameid).collection(u'users').document(i)
                    transaction.update(u, {"nextProposal": now + 15000})
                if senderid != '':
                    u = db.collection(u'games').document(gameid).collection(u'users').document(senderid)
                    transaction.update(u, {"nextProposal": now + 30000})
                transaction.set(new_item, {
                'history': history,
                'result': 'Rejected',
                'taskName': taskName
                })
                u = db.collection(u'games').document(gameid)
                transaction.update(u, {u'pendingPropose': 0})
            elif out_of_date and len(history) > 0:
                for p in ids:
                    g = db.collection(u'games').document(gameid).collection(u'pendingPropose').document(p)
                    transaction.delete(g)
                for i in uids:
                    u = db.collection(u'games').document(gameid).collection(u'users').document(i)
                    transaction.update(u, {"nextProposal": now + 15000})
                if senderid != '':
                    u = db.collection(u'games').document(gameid).collection(u'users').document(senderid)
                    transaction.update(u, {"nextProposal": now + 30000})
                transaction.set(new_item, {
                'history': history,
                'result': 'Out of Date',
                'taskName': taskName
                })
                u = db.collection(u'games').document(gameid)
                transaction.update(u, {u'pendingPropose': 0})
            elif len(ids) > 0 :
                u = db.collection(u'games').document(gameid)
                transaction.update(u, {u'pendingPropose': 1})
            elif len(ids) == 0:
                u = db.collection(u'games').document(gameid)
                transaction.update(u, {u'pendingPropose': 0})
            return True
        except Exception as ex:
            print(ex)
            return False

    return check(transaction, gameid)



def check_endgame(gameid):
    db = firestore.client()
    transaction = db.transaction()
    @firestore.transactional
    def check(transaction, gameid):
        try:
            game = db.collection(u'games').document(gameid).get(transaction=transaction).to_dict()
            roomid = game[u'roomid']
            timeStart = game[u'timeStart']
            now = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)
            if now - timeStart > 5 * 60 * 1000:
                all_users = db.collection(u'games').document(gameid).collection(u'users').get(transaction=transaction)
                for _p in all_users:
                    p = _p.to_dict()
                    i = p[u'uid']
                    score = p[u'score']
                    uu = db.collection(u'users').document(i)
                    s = db.collection(u'users').document(i).collection(u'scores').document(gameid)
                    transaction.update(uu, {u'all_games.' + roomid: {u'gameid' : "", u'weight': 0}})
                    transaction.set(s, {"score": score})
            return True
        except Exception as ex:
            print(ex)
            return False

    return check(transaction, gameid)
