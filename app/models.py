from django.db import models
import uuid
# Create your models here.
class Game(models.Model):
    gameid = models.UUIDField(default=uuid.uuid4, editable=False)
    weight = models.IntegerField()
    time = models.DecimalField(default=1000000000000.000,max_digits=16, decimal_places=3)
