# Generated by Django 2.0.1 on 2018-06-03 16:58

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gameid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('weight', models.IntegerField()),
            ],
        ),
    ]
