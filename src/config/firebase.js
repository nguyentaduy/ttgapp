var firebase = require("firebase/app");

// Add additional services you want to use
require("firebase/auth");
require("firebase/firestore");

import config from './firebase2.config'
firebase.initializeApp(config)

export default firebase
