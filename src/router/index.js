import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '../components/HelloWorld'
import GameStart from '../components/GameStart'
import GameRoom from '../components/GameRoom'
import GameEnd from '../components/GameEnd'
import Landing from '../components/Landing'
import Tutorial from '../components/Tutorial'
import SignUp from '../components/SignUp'
import Quiz from '../components/Quiz'
import Term from '../components/Term'
import Feedback from '../components/Feedback'
Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // },
    {
      path: '/',
      name: 'GameStart',
      component: GameStart
    },
    {
      path: '/gameRoom/:uid/:roomid/:gameid/:weight/:time',
      name: 'GameRoom',
      component: GameRoom,
      props: true
    },
    {
      path: '/gameEnd/:score/:uid',
      name: 'GameEnd',
      component: GameEnd,
      props: true
    },
    {
      path: '/hello',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/tutorial',
      name: 'Tutorial',
      component: Tutorial
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz
    },
    {
      path: '/term',
      name: 'Term',
      component: Term
    },
    {
      path: '/feedback',
      name: 'Feedback',
      component: Feedback
    },

  ]
})
