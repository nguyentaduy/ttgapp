import Vue from 'vue'
import Vuex from 'vuex'
import firebase from '../config/firebase'
const firestore = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);
Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    db: firestore,
    userList: [],
    gameId: '',
    profitList: [],
    currentTaskName: '',
    proposalWindowList: [],
    proposalHistory: [],
    pendingProposal: [],
    proposalTimerStart: false,
    uid: '',
    userName: '',
    isEndGame: false,
    proposalSender: '',
    userScore: 0,
    userStamina: 0,
    status: 'inPlay',
    totalWeight: 0,
    minThreshold: 0,
    isLoggedIn: false,
    consented: false,
    doneQuiz: false,
    nextProposal: 0,
  },
  getters: {},
  mutations: {
    initGame(state) {
      state.userList = []
      state.profitList = []
      state.currentTaskName = ''
      state.proposalWindowList = []
      state.proposalHistory = [],
      state.pendingProposal = [],
      state.proposalTimerStart = false,
      state.isEndGame = false,
      state.proposalSender = '',
      state.userScore = 0,
      state.userStamina = 0,
      state.status = 'inPlay',
      state.totalWeight = 0,
      state.minThreshold = 0
    },
    updateUserList(state, userList) {
      state.userList = userList
      var totalWeight = 0
      for (var i in userList){
        var user = userList[i]
        if (user.uid === state.uid) {
          state.userName = user.name
          state.userStamina = user.stamina
          state.status = user.status
          state.userScore = user.score,
          state.nextProposal = user.nextProposal
        }
        if (user.status === "inPlay") {
          totalWeight += user.stamina
        }
      }
      state.totalWeight = totalWeight
    },
    storeGameInfo(state, game) {
      state.gameId = game.gameId
      state.uid = game.uid
    },
    storeProfitList(state, thresholds) {
      state.profitList = thresholds.profitList
      state.minThreshold = thresholds.minThreshold
    },
    setCurrentTaskName(state, taskName) {
      state.currentTaskName = taskName
    },
    resetProposalWindowList(state) {
      state.proposalWindowList.forEach(proposeElement => {
        proposeElement.share = 0
      })
    },
    storeProposalWindowList(state, proposalWindowList) {
      state.proposalWindowList = proposalWindowList
    },
    storeProposalHistory(state, proposalHistory) {
      state.proposalHistory = proposalHistory
    },
    storePendingProposal(state, pendingProposal) {
      state.pendingProposal = pendingProposal
      if (pendingProposal.length > 0) {
        state.proposalSender = pendingProposal[0].sender
      }
    },
    startProposalTimer(state, signal) {
      state.proposalTimerStart = signal
    },
    writeScore(state, score) {
      state.userScore = score
    },
    toEndGame(state, bool) {
      state.isEndGame = bool
    },
    updateLogin(state, bool) {
      state.isLoggedIn = bool
    },
    updateConsent(state, bool) {
      state.consented = bool
    },
    updateQuiz(state, bool) {
      state.doneQuiz = bool
    },
    updateUid(state, uid) {
      state.uid = uid
    },
  },
  actions: {
  }
})
