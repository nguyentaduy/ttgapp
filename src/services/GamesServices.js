import axios from 'axios'
export default {
  fetchGame (userid, roomid) {
    return axios.get('assign?userid=' + userid + '&roomid=' + roomid);
  },
  checkProposal (gameid) {
    return axios.get('checkProposal?gameid=' + gameid);
  },
  checkEndGame (gameid) {
    return axios.get('checkEndGame?gameid=' + gameid);
  }
}
